// // Check if Number is Prime
// const checkPrime = (val) => {
//   // console.log("val", val);
//   let count = 0;
//   for (i = 2; i <= val / 2; i++) {
//     if (val % i === 0) count += 1;
//   }
//   if (count === 0) {
//     return true;
//   } else {
//     return false;
//   }
// };

// // Check if number is Cyclic prime
// const checkCyclic = (num) => {
//   // console.log("Number: ", num);
//   let permuts = permutator(num);

//   if (permuts.length === 0) {
//     return false;
//   }

//   return true;
// };

// // Get all the cyclic permutations
// function permutator(N) {
//   var num = N;
//   let n = N.toString().length;
//   let res = [];
//   while (true) {
//     // console.log(checkPrime(num));
//     if (!checkPrime(num)) return [];
//     res.push(num);
//     // console.log(num, res);

//     var rem = num % 10;
//     var dev = parseInt(num / 10);
//     num = parseInt(Math.pow(10, n - 1) * rem + dev);
//     if (num == N) break;
//   }
//   return res;
// }
// let start = new Date();
// // Get all the prime numbers in the range
// // let primes = [];
// let res = [];

// for (let i = 100; i < 1000000; i++) {
//   // if (checkPrime(i)) {
//   //   primes.push(i);
//   // }
//   if (checkCyclic(i)) {
//     res.push(i);
//   }
// }

// // Main function
// // primes.map((prime) => {
// //   if (checkCyclic(prime)) {
// //     res.push(prime);
// //   }
// // });

// let end = new Date();
// console.log(res, res.length);
// console.log((end.getTime() - start.getTime()) / 1000 + "sec");

// // 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97,

// // --------------------------------------------------------------------------------------

// // [
// //     113,    131,    197,    199,    311,
// //     337,    373,    719,    733,    919,
// //     971,    991,   1193,   1931,   3119,
// //    3779,   7793,   7937,   9311,   9377,
// //   11939,  19391,  19937,  37199,  39119,
// //   71993,  91193,  93719,  93911,  99371,
// //  193939, 199933, 319993, 331999, 391939,
// //  393919, 919393, 933199, 939193, 939391,
// //  993319, 999331
// // ] 42

// // --------------------------------------------------------------------------------------

// Check if Number is Prime
const checkPrime = (val) => {
  let count = 0;
  for (let i = 2; i < val / 2; i++) {
    if (val % i === 0) count += 1;
  }
  if (count == 0) return true;
  return false;
};

// Check if number is Cyclic prime
const checkCyclic = (num) => {
  // console.log("Number: ", num);
  let permuts = permutator(num);

  if (permuts.length === 0) {
    return false;
  }

  return true;
};

// Get all the cyclic permutations
function permutator(N) {
  var num = N;
  let n = N.toString().length;
  let res = [];
  while (true) {
    // console.log(checkPrime(num));

    res.push(num);
    // console.log(num, res);

    var rem = num % 10;
    var dev = parseInt(num / 10);
    num = parseInt(Math.pow(10, n - 1) * rem + dev);
    if (!primes.includes(num)) return [];
    if (num == N) break;
  }
  return res;
}
const start = new Date();

// Main function
let primes = [];
let res = [];

// NEW METHOD
const arr = Array.from(Array(1000000), (e, i) => i + 100);
arr.map((item) => {
  if (checkPrime(item)) {
    primes.push(item);
  }
});

primes.map((item) => {
  if (checkCyclic(item)) {
    res.push(item);
  }
});

// OLD METHOD
// for (let i = 100; i <= 100000; i++) {
//   if (checkPrime(i)) {
//     res.push(i);
//   }
// }

const end = new Date();
console.log(res, res.length);
console.log((end.getTime() - start.getTime()) / 1000 + "sec");

// 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, 97,
