let num = "aab";

const permute = (nums) => {
  let result = [];
  if (nums.length === 0) return [];
  if (nums.length === 1) return [nums];
  for (let i = 0; i < nums.length; i++) {
    const currentNum = nums[i];
    const remainingNums = nums.slice(0, i).concat(nums.slice(i + 1));
    const remainingNumsPermuted = permute(remainingNums);
    for (let j = 0; j < remainingNumsPermuted.length; j++) {
      const permutedArray = [currentNum].concat(remainingNumsPermuted[j]);
      result.push(permutedArray);
    }
  }
  return result;
};

function compare(result) {
  let arr = [];
  for (i = 0; i < result.length; i++) {
    let flag = true;
    for (j = 0; j < result[i].length; j++) {
      if (result[i][j] === result[i][j + 1]) {
        flag = false;
      }
    }
    if (flag) arr.push(result[i].join(""));
  }

  return { array: arr, len: arr.length };
}

let permmutations = permute(num.split(""));

console.log(compare(permmutations));


