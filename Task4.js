function bubbleSort(arr) {
  var i, j;
  var len = arr.length;

  var isSwapped = false;

  for (i = 0; i < len; i++) {
    isSwapped = false;

    for (j = 0; j < len; j++) {
      if (arr[j] > arr[j + 1]) {
        var temp = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = temp;
        isSwapped = true;
      }
    }

    // IF no two elements were swapped by inner loop, then break

    if (!isSwapped) {
      break;
    }
  }

  // Print the array
  console.log(arr);
}
var arr = [5, 45, 23, 1, 3, 20, 8, 10];
let sum = 1.5;
// calling the bubbleSort Function
bubbleSort(arr);
comparevalue(arr, sum);
function comparevalue(arr, sum) {
  let lastIndex;
  for (i = 0; i < arr.length; i++) {
    if (arr[i] < sum) {
      // console.log("Result", arr[i]);
      lastIndex = i;
    }
  }
  console.log("Index", ++lastIndex);
  // console.log(arr, sum);
}
